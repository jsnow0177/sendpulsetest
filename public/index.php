<?php
/**
 * Скрипт является точкой входа в консольное приложение-парсер
 */
namespace SPT;

use SPT\App\ParsingApp;
use SPT\Di\IServiceLocator;

define('APP_PATH', dirname(__FILE__));

/** @var IServiceLocator $serviceLocator */
include_once('container.php');

// Создаём и запускаем приложение
$app = new ParsingApp($serviceLocator);
$app->run($argv);