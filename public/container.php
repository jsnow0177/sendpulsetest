<?php
namespace SPT;
use SPT\App\SimpleParserFactory;
use SPT\Configuration\IConfig;
use SPT\Configuration\PhpFileConfig;
use SPT\Database\MySqliAdapter;
use SPT\Database\PDOAdapter;
use SPT\Di\IServiceLocator;
use SPT\Di\ServiceLocator;

$serviceLocator = new ServiceLocator();

$serviceLocator->set('config', function(){
    return PhpFileConfig::load(APP_PATH . '/config.php');
});

$serviceLocator->set('pdo', function(){
    /**
     * @var IServiceLocator $this
     * @var IConfig $config
     */
    $config = $this->get('config');
    $dsn = "mysql:host=%s;dbname=%s";
    $dsn = sprintf($dsn, $config->get('db.host'), $config->get('db.name'));

    $pdo = new \PDO($dsn, $config->get('db.user'), $config->get('db.pass'), [
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    ]);

    return $pdo;
});

$serviceLocator->set('parser', function(){
    return new SimpleParserFactory();
});

$serviceLocator->set('model', function($classname){
    /**
     * @var IServiceLocator $this
     * @var \PDO $pdo
     */
    $pdo = $this->get('pdo');

    return new $classname($pdo);
}, true); // Usage: $serviceLocator->model(MyAwesomeModel::class);

$serviceLocator->set('db', function(){
    /**
     * @var IServiceLocator $this
     * @var IConfig $config
     */
    $config = $this->get('config');
    $driver = strtolower($config->get('db.driver'));

    // Это тоже можно вынести в фабрику, например и сделать более абстрактным, но в данном случае, для демонстрации и этого хватит
    if($driver === 'pdo'){
        /** @var \PDO $pdo */
        $pdo = $this->get('pdo');
        return new PDOAdapter($pdo);
    }else{
        /** @var \mysqli $mysqli */
        $mysqli = $this->get('mysqli');
        return new MySqliAdapter($mysqli);
    }
});