<?php
namespace SPT\App\Models;

use SPT\SimpleModel;

class Services extends SimpleModel{

    /**
     * @param array $rows
     * @return array
     */
    protected function prepareRows(array $rows): array
    {
        return array_map(function($row){
            $row['id'] = (int)$row['id'];
            $row['frequency'] = (int)$row['frequency'];

            return $row;
        }, $rows);
    }

    /**
     * @return array
     */
    public function getReadyServices(): array
    {
        $stmt = $this->pdo->query("SELECT * FROM `services` WHERE `requested_at`>=NOW()");
        if($stmt !== false && ($rows = $stmt->fetchAll(\PDO::FETCH_ASSOC)) !== false)
            return $this->prepareRows($rows);

        return [];
    }

    /**
     * @param int $serviceId
     * @param $time
     */
    public function updateRequestedAt(int $serviceId, $time)
    {
        $stmt = $this->pdo->prepare("UPDATE `services` SET `requested_at`=? WHERE `id`=?");
        $stmt->execute([$time, $serviceId]);
    }

}