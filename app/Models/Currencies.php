<?php
namespace SPT\App\Models;

use SPT\SimpleModel;

class Currencies extends SimpleModel
{

    /**
     * @param int $service_id
     * @param array $currencies
     */
    public function updateCurrencies(int $service_id, array $currencies)
    {
        $stmt = $this->pdo->prepare("INSERT INTO `currencies` SET `service_id`=?, `currency_code`=?, `value`=? ON DUPLICATE KEY UPDATE `value`=?");

        foreach($currencies as $currency)
            $stmt->execute([$service_id, $currency['code'], $currency['value'], $currency['value']]);
    }

}