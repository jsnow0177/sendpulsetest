<?php
namespace SPT\App;

interface IParser{

    /**
     * Метод ДОЛЖЕН выполнять парсинг контента и возвращать набор валют либо выбрасывать исключение в случае, если парсинг невозможен
     * @param string $content
     * @return array
     * @throws \RuntimeException Исключение выбрасывается в случае, если парсинг невозможен
     */
    public function parse(string $content): array;

}