<?php
namespace SPT\App;

interface IRequest{

    /**
     * Устанавливает путь к кастомному файлу сертификатов
     * @param string $path
     * @return IRequest
     */
    public function setCustomCAInfoPath(string $path): IRequest;

    /**
     * Возвращает путь к кастомному файлу сертификатов
     * @return string
     */
    public function getCustomCAInfoPath(): string;

    /**
     * Включает проверку сертификата безопасности
     * @return IRequest
     */
    public function enableSSLVerification(): IRequest;

    /**
     * Отключает проверку сертификата безопасности
     * @return IRequest
     */
    public function disableSSLVerification(): IRequest;

    /**
     * Проверяет включена ли проверка сертификата
     * @return bool
     */
    public function isSSLVerificationEnabled(): bool;

    /**
     * Устанавливает User-Agent
     * @param string $userAgent
     * @return IRequest
     */
    public function setUserAgent(string $userAgent): IRequest;

    /**
     * Возвращает User-Agent
     * @return string
     */
    public function getUserAgent(): string;

    /**
     * Метод ДОЛЖЕН устанавливать URL цели запроса
     * @param string $url
     * @return $this
     */
    public function setURL(string $url): IRequest;

    /**
     * Метод ДОЛЖЕН возвращать URL
     * @return string
     */
    public function getURL(): string;

    /**
     * Принудительно включает режим POST-запросов
     * @return IRequest
     */
    public function enablePost(): IRequest;

    /**
     * Отключает принудительный режим POST-запроса
     * @return IRequest
     */
    public function disablePost(): IRequest;

    /**
     * Проверяет, включён ли принудительный режим POST-запроса
     * @return bool
     */
    public function isPost(): bool;

    /**
     * Устанавливает данные, отправляемые с помощью POST-запроса
     * @param array $postData
     * @return IRequest
     */
    public function setPostData(array $postData): IRequest;

    /**
     * Возвращает данные, установленные ранее с помощью ::setPostData
     * @see IRequest::setPostData
     * @return array
     */
    public function getPostData(): array;

    /**
     * Метод ДОЛЖЕН выполнять запрос
     * @return $this
     */
    public function exec(): IRequest;

    /**
     * Метод возвращает тело ответа.
     * Вернёт пустую строку, если запрос ещё не выполнен
     * @return string
     */
    public function getResult(): string;

}