<?php
namespace SPT\App;

class SimpleRequest implements IRequest{

    /**
     * @var string
     */
    private $customCAInfoPath = '';

    /**
     * @var bool
     */
    private $sslVerificationEnabled = true;

    /**
     * @var string
     */
    private $userAgent = '';

    /**
     * @var string
     */
    private $url = '';

    /**
     * @var bool
     */
    private $forcePOST = false;

    /**
     * @var array
     */
    private $postData = [];

    /**
     * @var string
     */
    private $result = '';

    /**
     * Устанавливает путь к кастомному файлу сертификатов
     * @param string $path
     * @return IRequest
     */
    public function setCustomCAInfoPath(string $path): IRequest
    {
        $this->customCAInfoPath = $path;
        return $this;
    }

    /**
     * Возвращает путь к кастомному файлу сертификатов
     * @return string
     */
    public function getCustomCAInfoPath(): string
    {
        return $this->customCAInfoPath;
    }

    /**
     * Включает проверку сертификата безопасности
     * @return IRequest
     */
    public function enableSSLVerification(): IRequest
    {
        $this->sslVerificationEnabled = true;

        return $this;
    }

    /**
     * Отключает проверку сертификата безопасности
     * @return IRequest
     */
    public function disableSSLVerification(): IRequest
    {
        $this->sslVerificationEnabled = false;

        return $this;
    }

    /**
     * Проверяет включена ли проверка сертификата
     * @return bool
     */
    public function isSSLVerificationEnabled(): bool
    {
        return $this->sslVerificationEnabled;
    }

    /**
     * Устанавливает User-Agent
     * @param string $userAgent
     * @return IRequest
     */
    public function setUserAgent(string $userAgent): IRequest
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * Возвращает User-Agent
     * @return string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }

    /**
     * Метод устанавливает URL цели запроса
     * @param string $url
     * @return $this
     */
    public function setURL(string $url): IRequest
    {
        $this->url = $url;
        return $this;
    }

    /**
     * Метод возвращает URL
     * @return string
     */
    public function getURL(): string
    {
        return $this->url;
    }

    /**
     * Принудительно включает режим POST-запросов
     * @return IRequest
     */
    public function enablePost(): IRequest
    {
        $this->forcePOST = true;
        return $this;
    }

    /**
     * Отключает принудительный режим POST-запроса
     * @return IRequest
     */
    public function disablePost(): IRequest
    {
        $this->forcePOST = false;
        return $this;
    }

    /**
     * Проверяет, включён ли принудительный режим POST-запроса
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->forcePOST;
    }

    /**
     * Устанавливает данные, отправляемые с помощью POST-запроса
     * @param array $postData
     * @return IRequest
     */
    public function setPostData(array $postData): IRequest
    {
        $this->postData = $postData;

        return $this;
    }

    /**
     * Возвращает данные, установленные ранее с помощью ::setPostData
     * @return array
     * @see IRequest::setPostData
     */
    public function getPostData(): array
    {
        return $this->postData;
    }

    /**
     * Метод выполняет запрос
     * @throws \RuntimeException
     * @return $this
     */
    public function exec(): IRequest
    {
        $ch = $this->prepare_curl();
        $content = curl_exec($ch);
        $errorCode = curl_errno($ch);
        $errorMsg = curl_error($ch);

        curl_close($ch);

        if($errorCode != 0)
            throw new \RuntimeException("Error while requesting api: {$errorMsg}");

        if($content)
            $this->result = $content;

        return $this;
    }

    /**
     * @return resource
     */
    protected function prepare_curl()
    {
        $ch = curl_init($this->getURL());
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => $this->isSSLVerificationEnabled(),
            CURLOPT_SSL_VERIFYHOST => $this->isSSLVerificationEnabled() ? 2 : 0
        ]);

        if($this->getCustomCAInfoPath() !== '')
            curl_setopt($ch, CURLOPT_CAINFO, $this->getCustomCAInfoPath());

        if(!empty($this->postData) || $this->isPost())
            curl_setopt($ch, CURLOPT_POST, true);

        if(!empty($this->postData))
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postData);

        return $ch;
    }

    /**
     * Метод возвращает тело ответа.
     * Вернёт пустую строку, если запрос ещё не выполнен
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

}