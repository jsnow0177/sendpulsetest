<?php
namespace SPT\App;

class Service{

    /**
     * @var IRequest
     */
    public $request;

    /**
     * @var IParser
     */
    public $parser;

    public function __construct(IRequest $request, IParser $parser)
    {
        $this->request = $request;
        $this->parser = $parser;
    }

    /**
     * Метод возвращает набор валют
     * @return array
     */
    public function getCurrencies(): array
    {
        try{
            $this->request->exec();
        }catch(\RuntimeException $ex){
            throw new \RuntimeException("Error occurred on request", 0, $ex);
        }

        $content = $this->request->getResult();

        try{
           $currencies = $this->parser->parse($content);
        }catch(\RuntimeException $ex){
            throw new \RuntimeException("Error occurred on parsing", 1, $ex);
        }

        return $currencies;
    }

}