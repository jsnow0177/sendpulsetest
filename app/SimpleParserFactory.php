<?php
namespace SPT\App;

use SPT\App\Parsers\JSONParser;
use SPT\App\Parsers\XMLParser;

class SimpleParserFactory{

    /**
     * @param string $type
     * @return IParser
     */
    public function factory(string $type): IParser
    {
        $type = strtolower($type);

        switch($type){
            case 'xml': return new XMLParser();
            case 'json': return new JSONParser();
            default: throw new \InvalidArgumentException("Invalid type specified; expected: json or xml, got: {$type}");
        }
    }

}