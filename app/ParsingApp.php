<?php
declare(strict_types=1);

namespace SPT\App;

use SPT\AbstractApplication;
use SPT\App\Models\Services;
use SPT\Configuration\IConfig;

class ParsingApp extends AbstractApplication{

    /**
     * @var Services
     */
    private $services;

    /**
     * @param array $processedArgs
     * @return mixed
     */
    protected function Main(array $processedArgs = [])
    {
        /** @var IConfig $config */
        $config = $this->serviceLocator->get('config');
        /** @var Services $services */
        $this->services = $services = $this->serviceLocator->model->get(Services::class);

        $servicesList = $services->getReadyServices();

        if(count($servicesList) === 0)
            die("Нет сервисов для опроса в очереди");

        foreach($servicesList as $service)
            $this->processService($service);
    }

    protected function processService(array $service)
    {
        $currencies = $this->getCurrencies($service['link'], $service['type']);
        if(count($currencies) === 0)
            return;

        $this->updateCurrencies($service['id'], $currencies);
        $this->updateService($service['id']);
    }

    protected function getCurrencies(string $link, string $type)
    {
        $request = new SimpleRequest();
        $request->setURL($link);
        $request->enablePost();
        $request->enableSSLVerification();

        try{
            $parser = $this->serviceLocator->get('parser')->factory($type);
        }catch(\InvalidArgumentException $ex){
            // TODO: Можно залоггировать ошибку или сделать ещё что-нибудь
            return [];
        }

        $worker = new Service($request, $parser);

        try{
            $currencies = $worker->getCurrencies();
        }catch(\Exception $ex){
            // TODO: Опять же, в зависимости от конкретной реализации, можно логгировать ошибку или ещё что-нибудь делать
            return [];
        }

        return $currencies;
    }

    protected function updateCurrencies(int $service_id, array $currencies)
    {
        // TODO: Код обновления значений валют
    }

    protected function updateService(int $service_id)
    {
        // TODO: Код обновления даты последнего запроса к сервису
    }

}