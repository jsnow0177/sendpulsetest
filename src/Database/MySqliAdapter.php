<?php
namespace SPT\Database;

class MySqliAdapter implements IDatabase{

    /**
     * @var \mysqli
     */
    private $mysqli;

    /**
     * MySqliAdapter constructor.
     * @param \mysqli $mysqli
     */
    public function __construct(\mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function query(string $query)
    {
        // TODO: реализация метода query для mysqli
        $this->mysqli->query($query); // как-то так
    }

}