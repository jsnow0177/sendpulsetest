<?php
namespace SPT\Database;

class PDOAdapter implements IDatabase{

    /**
     * @var \PDO
     */
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function query(string $query){
        // TODO: Реализация метода работы с запросом для PDO
        $this->pdo->query($query); // как-то так
    }

}