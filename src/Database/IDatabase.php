<?php
namespace SPT\Database;

interface IDatabase{

    /**
     * @param string $query
     * @return array Результаты запроса
     */
    public function query(string $query);

}