<?php
namespace SPT\Di;

/**
 * Class NotFactoryException
 * @package SPT\Di
 */
class NotFactoryException extends \RuntimeException{

}