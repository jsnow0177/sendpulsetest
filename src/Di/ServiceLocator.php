<?php
namespace SPT\Di;

class ServiceLocator implements IServiceLocator{

    // TODO: Реализация Service Locator

    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __call($name, $arguments)
    {
        if(!$this->isFactory($name))
            throw new \RuntimeException("Can't use non-factory dependency as factory");

        return $this->make($name, $arguments);
    }

}