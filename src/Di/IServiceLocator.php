<?php
namespace SPT\Di;

interface IServiceLocator{

    /**
     * Метод ДОЛЖЕН добавить в контейнер определение конкретного сервиса
     * Метод ДОЛЖЕН возвращать $this
     * @param string $name Имя зависимости добавляемой в контейнер, по которому эта зависимость будет доступна
     * @param \Closure|callable|object $definition Определение зависимости
     * @param bool $isFactory Определяет, является ли определение фабрикой (это необходимо для возможности сконструировать зависимость, передав ей какие-либо параметры)
     *
     * @return $this
     * @throws \InvalidArgumentException Исключение выбрасывается в случае, если передан некорректный аргумент $definition
     * @throws \RuntimeException Исключение выбрасывается в случае, когда зависимость с таким именем уже была ранее добавлена в контейнер
     */
    public function set(string $name, $definition, bool $isFactory = false): IServiceLocator;

    /**
     * Метод ДОЛЖЕН возвращать объект зависимости из контейнера, либо null, если в контейнере нет зависимости с именем $name
     * @param string $name
     * @return object|null
     */
    public function get(string $name): ?object;

    /**
     * Метод ДОЛЖЕН возвращать СВЕЖИЙ объект зависимости из контейнера, либо null, если в контейнере нет зависимости с именем $name
     * @param string $name
     * @return object|null
     */
    public function getFresh(string $name): ?object;

    /**
     * Метод ДОЛЖЕН возвращать объект, созданный фабрикой
     * @param string $name
     * @param array $args
     *
     * @throws NotFactoryException Исключение выбрасывается в случае, когда зависимость не является фабрикой
     * @return object
     */
    public function make(string $name, array $args = []): object;

    /**
     * Метод проверяет наличие зависимости в контейнере
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool;

    /**
     * Метод проверяет, является ли зависимость фабрикой
     * @param string $name
     * @return bool
     */
    public function isFactory(string $name): bool;

}