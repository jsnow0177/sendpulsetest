<?php
namespace SPT\Configuration;

class JsonFileConfig extends AbstractFileConfig{

    /**
     * @param string $filePath
     * @return AbstractFileConfig
     * @throws \InvalidArgumentException Это исключение выбрасывается когда передан некорректный путь к конфигурационному json-файлу
     * @throws \RuntimeException Это исключение выбрасывается когда не удаётся загрузить конфигурацию из json-файла
     */
    public static function load(string $filePath): AbstractFileConfig
    {
        if(!file_exists($filePath))
            throw new \InvalidArgumentException("Invalid path specified: {$filePath}");

        $json = file_get_contents($filePath);
        if(!$json)
            throw new \RuntimeException("Unable to read data from configuration file: {$filePath}");

        $config = json_decode($json, true);

        if(!is_array($config) || json_last_error() !== JSON_ERROR_NONE)
            throw new \RuntimeException("Invalid configuration file format: {$filePath}");

        return new static($config);
    }

}