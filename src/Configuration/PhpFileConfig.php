<?php
namespace SPT\Configuration;

class PhpFileConfig extends AbstractFileConfig{

    /**
     * @param string $filePath
     * @return PhpFileConfig
     * @throws \InvalidArgumentException Это исключение выбрасывается когда передан некорректный путь к конфигурационному php-файлу
     * @throws \RuntimeException Это исключение выбрасывается когда не удалось получить конфигурационный массив из php-файла
     */
    public static function load(string $filePath): AbstractFileConfig{
        if(!file_exists($filePath))
            throw new \InvalidArgumentException("Invalid file path specified: {$filePath}");

        $config = @include($filePath);

        if(isset($_))
            $config = $_;

        if(!is_array($config))
            throw new \RuntimeException("Can\'t load configuration from php file");

        return new static($config);
    }

}