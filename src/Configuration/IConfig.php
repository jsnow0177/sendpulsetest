<?php
namespace SPT\Configuration;

/**
 * Interface IConfig
 * @package SPT\Configuration
 */
interface IConfig{

    /**
     * Метод ДОЛЖЕН установить параметр конфигурации с именем $name
     * Метод ДОЛЖЕН вернуть $this
     * @param string $name Имя параметра конфигурации
     * @param mixed $value Значение параметра конфигурации
     * @return $this
     */
    public function set(string $name, $value): IConfig;

    /**
     * Метод ДОЛЖЕН вернуть либо значение параметра конфигурации, либо $default, если такого значения нет
     * @param string $name
     * @param mixed|null $default
     * @return mixed|$default
     */
    public function get(string $name, $default = null);

    /**
     * Метод проверяет наличие параметра конфигурации
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool;

}