<?php
namespace SPT\Configuration;

class DatabaseConfig implements IConfig{

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * DatabaseConfig constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function __destruct()
    {
        // Освобождение ресурсов и тому подобное
    }

    // TODO: Реализация конфигурационного хранилища в БД

}