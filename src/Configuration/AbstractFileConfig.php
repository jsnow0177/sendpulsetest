<?php
namespace SPT\Configuration;

abstract class AbstractFileConfig implements IConfig{

    use TConfigCommonMethods;

    /**
     * @param string $filePath
     * @return AbstractFileConfig
     */
    public abstract static function load(string $filePath): AbstractFileConfig;

    /**
     * AbstractFileConfig constructor.
     * @param array $config
     */
    protected function __construct(array $config)
    {
        $this->config = $config;
    }

}