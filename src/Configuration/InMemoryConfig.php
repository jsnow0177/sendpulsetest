<?php
namespace SPT\Configuration;

class InMemoryConfig implements IConfig{

    use TConfigCommonMethods;

    /**
     * InMemoryConfig constructor.
     * @param array $initialConfig
     */
    public function __construct(array $initialConfig = [])
    {
        $this->setFromArray($initialConfig);
    }

    /**
     * @param array $config
     * @return $this
     */
    public function setFromArray(array $config): InMemoryConfig
    {
        $this->config = array_replace($this->config, $config);

        return $this;
    }

}