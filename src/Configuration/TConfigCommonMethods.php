<?php
namespace SPT\Configuration;

trait TConfigCommonMethods{

    /**
     * @var array
     */
    protected $config = [];

    /**
     * Метод ДОЛЖЕН установить параметр конфигурации с именем $name
     * Метод ДОЛЖЕН вернуть $this
     * @param string $name Имя параметра конфигурации
     * @param mixed $value Значение параметра конфигурации
     * @return $this
     */
    public function set(string $name, $value): IConfig
    {
        $this->config[$name] = $value;

        return $this;
    }

    /**
     * Метод ДОЛЖЕН вернуть либо значение параметра конфигурации, либо $default, если такого значения нет
     * @param string $name
     * @param mixed|null $default
     * @return mixed|$default
     */
    public function get(string $name, $default = null)
    {
        return $this->has($name) ? $this->config[$name] : $default;
    }

    /**
     * Метод проверяет наличие параметра конфигурации
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return isset($this->config[$name]);
    }

}