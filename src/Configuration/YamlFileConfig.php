<?php
namespace SPT\Configuration;

class YamlFileConfig extends AbstractFileConfig{

    use TConfigCommonMethods;

    /**
     * @param string $filePath
     * @return AbstractFileConfig
     */
    public static function load(string $filePath): AbstractFileConfig
    {
        // TODO: Реализация загрузки конфигурации из YAML-файла
        $config = [];
        return new static($config);
    }


}