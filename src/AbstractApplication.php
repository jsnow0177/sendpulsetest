<?php
namespace SPT;

use SPT\Di\IServiceLocator;

abstract class AbstractApplication{

    /**
     * @var IServiceLocator
     */
    protected $serviceLocator;

    /**
     * Метод обрабатывает аргументы командной строки и формирует из них более удобный для работы массив, где ключ - имя аргумента,
     * а значение - соответственно значение
     * @param array $args
     * @return array
     */
    public static function processArgs(array $args): array
    {
        // TODO: Реализация метода processArgs
        return $args;
    }

    /**
     * AbstractApplication constructor.
     * @param IServiceLocator $serviceLocator
     */
    public function __construct(IServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Метод подготавливает аргументы командной строки, переданные в приложение и подготавливает их, а затем передаёт
     * выполнение на точку входа приложения
     *
     * @param array $args
     * @return mixed
     */
    public function run(array $args = []){
        $processedArgs = static::processArgs($args);
        return $this->Main($processedArgs);
    }

    /**
     * Метод представляет из себя точку входа приложения (в терминологии MVC и веба - это FrontController, но с точки
     * зрения консольного программирования - это точка входа в приложение)
     *
     * Вся основная логика должна быть в этом методе, в конкретной реализации приложения
     *
     * @param array $processedArgs
     * @return mixed
     */
    protected abstract function Main(array $processedArgs = []);

}