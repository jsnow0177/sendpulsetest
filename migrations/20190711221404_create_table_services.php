<?php

use Phinx\Migration\AbstractMigration;

class CreateTableServices extends AbstractMigration
{

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('services', ['signed' => false])
            ->addColumn('name', 'string', ['length' => 32])
            ->addColumn('link', 'string', ['length' => 255])
            ->addColumn('type', 'set', ['values' => ['xml', 'json']])
            ->addColumn('frequency', 'integer', ['signed' => false, 'comment' => 'In seconds'])
            ->addColumn('requested_at', 'datetime')
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('services')
            ->drop();
    }

}