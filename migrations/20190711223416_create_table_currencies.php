<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCurrencies extends AbstractMigration
{
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('currencies', ['signed' => false])
            ->addColumn('service_id', 'integer', ['signed' => false])
            ->addColumn('currency_code', 'string', ['length' => 3])
            ->addColumn('value', 'decimal', ['precision' => 13, 'scale' => 4])
            ->addIndex(['service_id', 'currency_code'], ['unique' => true])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('currencies')
            ->drop();
    }
}